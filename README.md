This script will check for available appointments for COVID-19 vaccines from the https://www.vaccinespotter.org/ API.

Using the --state-code argument a state can be specified using the list on https://www.vaccinespotter.org/api/ ( defaults to MN )

Subsequent runs will not print out appointments already printed. The saved list is auto pruned as availability is removed. Meaning if the site opens up availability these will be reoutputted.

Example of usages:
    
    List the appointments available in Minnesota
    /usr/bin/python3 vaccine-appointments

    List the appointments available in Wisconsin
    /usr/bin/python3 vaccine-appointments --state-code WI
    
There are some gotchas to this script. It can be somewhat noisy and was quickly thrown together. The API is also considered beta and subject to change. Please submit issues if you run into a problem
I can't promise I'll make the fix immediately, but if I notice the issue I'll try to resolve it in a timely manner.

This script is made with no warranties or guarantees. It's free for you to use and distribute. I'm just giving this out publically to help eligible people get the vaccine.
